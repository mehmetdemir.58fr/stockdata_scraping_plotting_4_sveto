import requests
from bs4 import BeautifulSoup
import pandas as pd

html_data = requests.get("https://www.macrotrends.net/stocks/charts/GME/gamestop/revenue")
soup = BeautifulSoup(html_data.content, "html.parser")
table = soup.find_all("tbody")[1]
table_rows = table.find_all("tr")

gme_revenue = []

for tr in table_rows:
    td = tr.find_all('td')
    row = [cell.text for cell in td]
    gme_revenue.append(row)

gme_revenue = pd.DataFrame(gme_revenue, columns=["Date", "Revenue"])
gme_revenue["Revenue"] = gme_revenue['Revenue'].str.replace(',|\$',"")
gme_revenue.dropna(inplace=True)
gme_revenue = gme_revenue[gme_revenue['Revenue'] != ""]
print(gme_revenue.tail())